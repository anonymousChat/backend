--TABLE users
CREATE TABLE IF NOT EXISTS users (
  id BIGSERIAL PRIMARY KEY,
  username varchar(128) NOT NULL UNIQUE
);

--TABLE messages
CREATE TABLE IF NOT EXISTS messages (
  id BIGSERIAL PRIMARY KEY,
  message TEXT NOT NULL,
  user_id BIGINT NOT NULL,
  cdate TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES users (id)
);


