package com.doodle.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.core.env.Environment;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private Environment environment;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        String CORS_URL = "cors.url";
        registry.addMapping("/**")
                .allowedOrigins(environment.getProperty(CORS_URL))
                .allowedMethods("POST", "GET")
                .allowCredentials(false).maxAge(3600);

    }
}
