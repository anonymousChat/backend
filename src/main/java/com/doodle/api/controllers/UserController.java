package com.doodle.api.controllers;

import com.doodle.api.repository.User;
import com.doodle.api.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/users")
    public User createOrReturnExistingUser(@Valid @RequestBody User user) {
        Optional<User> maybeUser = userRepository.findByUsername(user.getUsername());
        return maybeUser.orElseGet(() -> {
                    logger.info(String.format("User does not exist with name %s. Creating a new user!", user.getUsername()));
                    return userRepository.save(user);
                }
        );
    }
}
