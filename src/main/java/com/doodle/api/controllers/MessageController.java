package com.doodle.api.controllers;

import com.doodle.api.exception.UserNotFoundException;
import com.doodle.api.model.ChatMessage;
import com.doodle.api.repository.Message;
import com.doodle.api.repository.MessageRepository;
import com.doodle.api.repository.User;
import com.doodle.api.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class MessageController {

    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

    private final MessageRepository messageRepository;
    private final UserRepository userRepository;
    private final SimpMessagingTemplate webSocket;

    @Autowired
    public MessageController(MessageRepository messageRepository,
                             UserRepository userRepository, SimpMessagingTemplate webSocket) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.webSocket = webSocket;
    }

    @GetMapping("/messages")
    public List<ChatMessage> getMessages(@RequestParam Optional<Long> offset, @RequestParam Integer limit) {
        List<Message> messages = messageRepository.findLast100MessagesWithTimeRange(getDateAsOffset(offset), limit);
        return messages.stream()
                .map(this::createChatMessageWithUserInfo)
                .collect(Collectors.toList());
    }

    @PostMapping("/messages")
    public ResponseEntity crateMessage(@Valid @RequestBody Message message) {
        Optional<User> currentUser = userRepository.getById(message.getUser_id());

        if (!currentUser.isPresent()) {
            throw new UserNotFoundException(String.format("User could not be found with id: %d", message.getUser_id()));
        }

        messageRepository.save(message);
        ChatMessage chatMessage = new ChatMessage(message, currentUser.get().getUsername());
        webSocket.convertAndSend("/message", chatMessage);
        logger.info(String.format("Message is sent to ws channel with id %d", message.getId()));
        return ResponseEntity.ok(chatMessage);
    }

    private Instant getDateAsOffset(Optional<Long> offset) {
        return offset
                .map(Instant::ofEpochMilli)
                .orElseGet(Instant::now);
    }

    private ChatMessage createChatMessageWithUserInfo(Message message) {
        return new ChatMessage(
                message,
                userRepository.getById(message.getUser_id()).map(User::getUsername).orElse(null)
        );
    }
}
