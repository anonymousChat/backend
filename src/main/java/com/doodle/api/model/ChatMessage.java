package com.doodle.api.model;

import com.doodle.api.repository.Message;

public class ChatMessage {

    private Long messageId;
    private String message;
    private String userName;
    private Long messageDate;

    public ChatMessage(Message message, String userName) {
        this.messageId = message.getId();
        this.message = message.getMessage();
        this.userName = userName;
        this.messageDate = message.getCdate().getTime();
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Long messageDate) {
        this.messageDate = messageDate;
    }
}
