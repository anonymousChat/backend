package com.doodle.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    @Query(value = "SELECT * FROM messages WHERE cdate < ?1 order by cdate desc limit ?2", nativeQuery = true)
    List<Message> findLast100MessagesWithTimeRange(Instant startDate, int limit);
}
