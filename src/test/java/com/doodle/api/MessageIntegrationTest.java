package com.doodle.api;

import com.doodle.api.repository.Message;
import com.doodle.api.repository.MessageRepository;
import com.doodle.api.repository.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.Matchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts="classpath:/test-sql/cleanup.sql")
@RunWith(SpringRunner.class)
@DataJpaTest
public class MessageIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MessageRepository messageRepository;

    @Test
    public void whenFindLast100MessagesWithTimeRange_thenReturnNewestMessages() {
        // given
        User user = new User();
        user.setUsername("marge");
        entityManager.persist(user);

        //persisted multiple messages with different times
        Message todaysMessage = createMessage("message1", user.getId(), new Date());
        Message yesterdaysMessage = createMessage("message2", user.getId(), getDate(1));
        Message theDayBeforeYesterdaysMessage = createMessage("message3", user.getId(), getDate(2));

        entityManager.persist(todaysMessage);
        entityManager.persist(yesterdaysMessage);
        entityManager.persist(theDayBeforeYesterdaysMessage);

        entityManager.flush();

        // when
        List<Message> messagesInThePast = messageRepository.findLast100MessagesWithTimeRange(getDate(1).toInstant(), 10);

        // then
        assertThat(messagesInThePast, not(hasItem(todaysMessage)));
    }

    private Message createMessage(String messageText, Long userId, Date messageDate) {
        Message message = new Message();
        message.setMessage(messageText);
        message.setCdate(messageDate);
        message.setUser_id(userId);
        return message;
    }

    private Date getDate(int days) {
        return Date.from(Instant.now().minus(days, ChronoUnit.DAYS));
    }

}
