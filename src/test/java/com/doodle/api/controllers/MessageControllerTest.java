package com.doodle.api.controllers;

import com.doodle.api.repository.Message;
import com.doodle.api.repository.MessageRepository;
import com.doodle.api.repository.User;
import com.doodle.api.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private MessageRepository messageRepository;
    @MockBean
    private SimpMessagingTemplate simpMessagingTemplate;

    @InjectMocks
    private MessageController messageController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        JacksonTester.initFields(this, new ObjectMapper());
        messageController = new MessageController(messageRepository, userRepository, simpMessagingTemplate);

        mvc = MockMvcBuilders.standaloneSetup(messageController)
                .build();
    }

    @Test
    public void returnMessages() throws Exception {
        // given
        int limit = 3;
        when(messageRepository.findLast100MessagesWithTimeRange(Instant.now(), limit))
                .thenReturn(givenMessages());

        // when
        MockHttpServletResponse response = mvc.perform(get("/messages?limit=" + limit))
                .andReturn().getResponse();

        // then
        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void return400IfRequiredQueryParameterDoesNotExistsForGettingMessages() throws Exception {
        // when
        MockHttpServletResponse response = mvc.perform(get("/messages"))
                .andReturn().getResponse();

        // then
        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void returns404IfUserDoesNotExistWhenNewMessageArrives() throws Exception {
        // given
        String json = "{\"message\": \"my message\",\"user_id\":12,\"cdate\": \"2018-05-16T13:36:38.000+0000\"}";
        when(userRepository.getById(givenUser().getId()))
                .thenReturn(Optional.empty());

        // when
        MockHttpServletResponse response = mvc.perform(post("/messages")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andReturn().getResponse();

        // then
        assertEquals(response.getStatus(), HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void returns200IfUserExistAndSavesNewMessageToDBWhenNewMessageArrives() throws Exception {
        // given
        String json = "{\"message\": \"my message\",\"user_id\":12,\"cdate\": \"2018-05-16T13:36:38.000+0000\"}";
        when(userRepository.getById(givenUser().getId()))
                .thenReturn(Optional.of(givenUser()));

        // when
        MockHttpServletResponse response = mvc.perform(post("/messages")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andReturn().getResponse();

        // then
        assertEquals(response.getStatus(), HttpStatus.OK.value());
        verify(messageRepository).save(any(Message.class));
    }

    @Test
    public void returns400IfJsonInvalidForSavingNewMessages() throws Exception {
        // given
        String json = "{\"invalidJson\": \"my message\"}";

        // when
        MockHttpServletResponse response = mvc.perform(post("/messages")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andReturn().getResponse();

        // then
        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
        verify(messageRepository, times(0)).save(any(Message.class));
    }

    private User givenUser() {
        User user = new User();
        user.setId(12L);
        user.setUsername("Marge");
        return user;
    }

    private Message givenMessage(Long messageId) {
        Message message = new Message();
        message.setId(messageId);
        message.setMessage("Here is my message");
        message.setUser_id(2L);
        message.setCdate(new Date());
        return message;
    }

    private List<Message> givenMessages() {
        List<Message> messages = new ArrayList<>();
        messages.add(givenMessage(1L));
        messages.add(givenMessage(2L));
        messages.add(givenMessage(3L));
        return messages;
    }
}

