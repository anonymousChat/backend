package com.doodle.api.controllers;

import com.doodle.api.repository.User;
import com.doodle.api.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    @InjectMocks
    private UserController userController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        JacksonTester.initFields(this, new ObjectMapper());
        userController = new UserController(userRepository);
        mvc = MockMvcBuilders.standaloneSetup(userController)
                .build();
    }

    @Test
    public void returnAlreadyExistingUserByName() throws Exception {
        // given
        String json = "{\"id\":1,\"username\":\"Marge\"}";
        User user = givenUser();
        when(userRepository.findByUsername(user.getUsername()))
                .thenReturn(Optional.of(user));

        // when
        MockHttpServletResponse response = mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andReturn().getResponse();

        // then
        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void createAndReturnUserIfDoesNotExist() throws Exception {
        // given
        String json = "{\"id\":1,\"username\":\"Marge\"}";
        User user = givenUser();
        when(userRepository.findByUsername(user.getUsername()))
                .thenReturn(Optional.empty());

        // when
        MockHttpServletResponse response = mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andReturn().getResponse();

        // then
        verify(userRepository).save(any(User.class));
        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void return400IfJsonInvalidForUsersEndpoint() throws Exception {
        // given
        String invalidJson = "{\"invalid\":12}";

        // when
        MockHttpServletResponse response = mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(invalidJson))
                .andReturn().getResponse();

        // then
        verify(userRepository, times(0)).save(any(User.class));
        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    private User givenUser() {
        User user = new User();
        user.setId(1L);
        user.setUsername("Marge");
        return user;
    }
}
