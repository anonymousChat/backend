package com.doodle.api;

import com.doodle.api.repository.User;
import com.doodle.api.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts="classpath:/test-sql/cleanup.sql")
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenGetById_thenReturnUser() {
        // given
        User user = new User();
        user.setUsername("marge");

        entityManager.persist(user);
        entityManager.flush();

        // when
        Optional<User> foundUser = userRepository.getById(user.getId());

        // then
        assertEquals(foundUser.get(), user);
    }

    @Test
    public void whenUserDoesNotExists_ReturnNone() {
        //given
        Long nonExistingUserId = 1234L;
        // when
        Optional<User> foundUser = userRepository.getById(nonExistingUserId);

        // then
        assertFalse(foundUser.isPresent());
    }

    @Test
    public void whenFindByUserName_thenReturnUser() {
        // given
        User user = new User();
        user.setUsername("marge");

        entityManager.persist(user);
        entityManager.flush();

        // when
        Optional<User> foundUser = userRepository.findByUsername(user.getUsername());

        // then
        assertEquals(foundUser.get(), user);
    }

    @Test
    public void whenUserNameDoesNotExists_ReturnNone() {
        //given
        String nonExistingUserName = "noName";
        // when
        Optional<User> foundUser = userRepository.findByUsername(nonExistingUserName);

        // then
        assertFalse(foundUser.isPresent());
    }
}

