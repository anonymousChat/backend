FROM openjdk:8-jdk-alpine

LABEL maintainer="seyda@benzer.me"

VOLUME /tmp

EXPOSE 8080

ARG JAR_FILE=dist/api-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} chat-backend.jar

ENTRYPOINT ["java","-jar","/chat-backend.jar"]
