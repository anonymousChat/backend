# Backend
> Chat Backend

## Requirements
1. Docker

## Setting up dev environment
Run `docker-compose up` and you're ready to go.
If you need to rebuild your image use `docker-compose build`

`docker-compose down && docker-compose build && docker-compose up`

## Dev ports

```
 8080: Spring-Boot Application
 ---
 5432: Postgres Database
 
```
 You can find the credentials in .env file

## Tech Stack

1. Spring Boot 
    - I choose Spring Boot, because it easy to use for creation REST application with powerful database capabilities 
2. Maven
3. PostgreSQL
    - I choose PostgreSQL, because I am familiar with it
4. Docker

## Postman Collection
1. You can find the runnable version of API endpoints with samples and doc on Postman. Here is the link
https://documenter.getpostman.com/view/74682/RzZ3LNNk#8416acc9-d832-4c7f-9efa-02b5c3991835

## Database Schema
![DatabaseSchema](docs/dbSchema.png)

## Notes

1. When you run the application, 2 tables will be created automatically via flyway migration scripts (users and messages)
2. It uses PostgresSQL's default database (postgres - public schema)
3. You can find the credentials in application.properties file

## Next Steps
1. Add authentication mechanism 
    - If we have authentication, we can easily get the current loggedIn user and check the access rights. 
    For now, a dummy authentication is implemented by using `user` table. Checking the user via database query.
    If user does not exist (assumed not logged in), creating a user (assumed logging in). 
    If we have a user with a given username, chat will be accessible for the user via username. 
    With this implementation, I can also clarify the HttpStatus of /users endpoint. If it is a user creation status
    will be 201, if it is a returning of an existing user, will be 200.
2. Add service layer between Controller and Repository
3. Encrypt the messages
4. Run the controller tests in Java 11. 
    - Integration tests are running without any issue with Java 11. But Controller tests are having issues. It's a known issue as I found from the 
    documentation. Mocking Repository classes and creating an test ApplicationContext having problems. So, I uses Java8 for running the tests.
5. Setup a production environment    
    
## Unit and Integration Tests

- Integration: UserRepository and MessageRepository are tested with integration tests. Running postgres (via docker)
is directly used during the test.
- Unit: Endpoints are covered with unit tests.

You can find the tests preview below.
![DatabaseSchema](docs/testsPreview.png)
    
    
    

